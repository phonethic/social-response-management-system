<?php
session_start();
require_once('src/Facebook/Entities/AccessToken.php');
require_once("src/Facebook/FacebookSession.php");


require_once('src/Facebook/HttpClients/FacebookHttpable.php');
require_once('src/Facebook/HttpClients/FacebookCurl.php');
require_once('src/Facebook/HttpClients/FacebookCurlHttpClient.php');
require_once("src/Facebook/FacebookRequest.php");
require_once("src/Facebook/FacebookResponse.php");

require_once("src/Facebook/FacebookSDKException.php");
require_once("src/Facebook/FacebookRequestException.php");
require_once("src/Facebook/FacebookPermissionException.php");
require_once("src/Facebook/FacebookAuthorizationException.php");
require_once("src/Facebook/FacebookRequestException.php");

require_once("src/Facebook/FacebookSignedRequestFromInputHelper.php");
require_once("src/Facebook/FacebookJavaScriptLoginHelper.php");
require_once("src/Facebook/FacebookRedirectLoginHelper.php");

require_once("src/Facebook/GraphObject.php");
require_once("src/Facebook/GraphUser.php");


use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\GraphObject;
use Facebook\GraphUser;
use Facebook\FacebookPermissionException;
use Facebook\FacebookRequestException;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookJavaScriptLoginHelper;


FacebookSession::setDefaultApplication('684236505017537','a9ce7fe0c7d023cb1a3de41ec3c87a7c');

// Use one of the helper classes to get a FacebookSession object.
//   FacebookRedirectLoginHelper
//   FacebookCanvasLoginHelper
//   FacebookJavaScriptLoginHelper
// or create a FacebookSession with a valid access token:
//$loginObj = new FacebookRedirectLoginHelper("stage.phonethics.in",'684236505017537','a9ce7fe0c7d023cb1a3de41ec3c87a7c'); 

$helper = new FacebookRedirectLoginHelper("http://stage.phonethics.in/social-response-management-system/index.php");
$session = $helper->getSessionFromRedirect();
if($session==""){
	//$session = $helper->getSessionFromRedirect();
	//header("Location:".$helper->getLoginUrl());
	echo '<a href="' . $helper->getLoginUrl(array('public_profile','email','user_friends','publish_actions')) . '">Login</a>';
}
// Get the GraphUser object for the current user:

try {
  $me = (new FacebookRequest(
    $session, 'GET', '/me'
  ))->execute()->getGraphObject(GraphUser::className());
  echo $me->getName();
} catch (FacebookRequestException $e) {
  // The Graph API returned an error
  print_r($e);
} catch (\Exception $e) {
  // Some other error occurred
  print_r($e);
}
?>